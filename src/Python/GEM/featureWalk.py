#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import math
import os
import time
import cTimer
import numpy as np
import pandas as pd
import networkx as nx

from sklearn.neighbors import BallTree
from sklearn.preprocessing import MinMaxScaler
from karateclub import Walklets
from sklearn.neighbors import kneighbors_graph

def check_binary(x):
    is_binary = True
    for v in np.nditer(x):
        if v.item() != 0 and v.item() != 1:
            is_binary = False
            break

    return is_binary



class feature_walk():
    """FeatureWalk is an algorithm to compute graph embeddings. It walks along the generate feature graphs for
    computing embeddings with Walklets"""
        
    def __init__(self, real_data, tlog, dim = 32, binary_data = None, ordinal_data = None, compute_split = True):        
        self.rdata = real_data
        self.bdata = binary_data
        self.odata = ordinal_data
        self.compute_split = compute_split        
        self.seed = 40
        self.dim = dim
        self.t = tlog
        print(f"Embedding Graph with featureWalk and {round(self.dim,0):f} dimensions")
        
    def train(self):        
        self.t.start("featureWalk preprocess time: ")
        self.preprocess()
        self.t.stop() 
        
        if self.compute_split is True:       
            self.t.start("featureWalk split embedding time: ")
            self.split_embedding()        
            self.t.stop()
        else: 
            print("skipping split embeddings")
        
        self.t.start("featureWalk full embedding time: ")
        self.total_embedding()
        self.t.stop()
        self.t.add_total() ## add total time
        print("Completion time: " + str(self.t.get_log()[-1][1])) # total time

        
    def preprocess(self):        
        scaler = MinMaxScaler(feature_range=(0,1))
        self.real_data = scaler.fit_transform(X=self.rdata)
        #self.binary_data = self.bdata
        #self.ordinal_data = scaler.fit_transform(X=self.odata)
        #self.full_data = np.concatenate((self.real_data,self.binary_data,self.ordinal_data), axis=1)
        self.full_data = self.real_data
        self.shape = np.shape(self.full_data)
    
    def gen_graph(self,data):
        start = time.time()
        data = BallTree(data, leaf_size=20)   
        # For 1D data add .reshape(-1, 1) to function call
        k = round(math.sqrt(self.shape[0])/5)
        k_Graph = kneighbors_graph(data, k, mode='distance', include_self=False, metric = "manhattan")
        k_Graph.data -=1 # reverse 0 and 1 range for weights
        k_Graph.data **= 3 # increase negative effect of longer distances
        G = nx.from_numpy_matrix(abs(k_Graph.A))
        G = nx.Graph(G.edges(data=True)) ##TEMP
        G = G.to_directed()
        print("Number of nodes " + str(len(G.nodes())) + " Number of edges: " + str(len(G.edges())))       
        return(G)
    
    def gen_embedding(self,graph,dim):
        model = None        
        model = Walklets(dimensions = dim, seed=self.seed,window_size=5,epochs=10,walk_number=10,walk_length=80)    
        model.fit(graph)
        X = model.get_embedding()
        return(X)
    
    def split_embedding(self):
        data = self.full_data
        shape = self.shape[1] # Number of features
        n_nodes = self.shape[0] # number of nodes
        dim = self.dim # specified dimensions
        
        emb_mat = np.zeros([shape, n_nodes, dim*5])#walklets multiplier    
        emb_mat = emb_mat.astype('float64')
        for i in range(shape):
            if check_binary(data[:,i]):
                print("is binary")
                emb_split = np.repeat(data[:,i].reshape(-1, 1),dim*5,axis=1) # Superfluous repetition, unfortunately required without changing structure of algorithm                
            else:                
                graph = self.gen_graph(data[:,i].reshape(-1, 1))
                print("Split embedding shape: " + str(np.shape(emb_mat[i])))
                emb_split = self.gen_embedding(graph,dim)
            emb_mat[i] = emb_split
        self.split_emb_mat = emb_mat
    
    def total_embedding(self):
        data = self.full_data
        shape = self.shape[1] # Number of features
        n_nodes = self.shape[0] # number of nodes
        dim = self.dim#*shape # specified dimensions times number of features
        
        emb_mat = np.zeros([n_nodes, dim*5]) #walklets multiplier (5 scale search)
        print("Full embedding shape: " + str(np.shape(emb_mat))) 
        emb_mat = emb_mat.astype('float64')
        graph = self.gen_graph(data)
        emb_mat = self.gen_embedding(graph,dim)
        self.full_emb_mat = emb_mat
        
    def get_full_mat(self):
        return(self.full_emb_mat)
    
    def get_split_mat(self):
        return(self.split_emb_mat)
    
    def cal_sav_struct(self, structG, save_path, nm): ## if you also want to run the structure embedding
        self.t.start("featureWalk structure time: ")        
        struct_emb = self.gen_embedding(structG,self.dim)
        structdata = pd.DataFrame(data=struct_emb)
        structdata.to_csv(save_path + "e_1_Struct_r_" + nm + ".txt")
        self.t.stop()
        self.t.add_total() ## add total time        
        
    
    def get_pandas(self, save = False, save_path = "", nm = ""): 
        n_feat = self.shape[1] # number of features
        embed_dim = n_feat*5*self.dim # total embedding dimensions        
        rng_feat = range(0,n_feat)
        n_nodes = self.shape[0]
        dim_split = range(0,self.dim*5)
        dim_total = range(0,self.dim*5)#*n_feat)
        
        
        full_mat = self.get_full_mat()
        full_cols = ['combined_dim_' + str(d+1) for d in dim_total]
        dataframe_full = pd.DataFrame(data=full_mat, columns=full_cols)
        
        if self.compute_split is True:
            split_mat = self.get_split_mat()  
            split_cols = ['feat_' + str(n+1) + '_dim_' + str(d+1) for n in rng_feat for d in dim_split]       
            split_embedding = np.reshape(split_mat,(n_nodes,embed_dim))    
            dataframe_split = pd.DataFrame(data=split_embedding, columns=split_cols)
            
            dataframe_concat = pd.concat([dataframe_full, dataframe_split], axis = 1)        

        
        
        if save: # For saving: combined embedding, structure embedding, feature embeddings            
            print("saving embedding to: ", save_path)
            dataframe_full.to_csv(save_path+"f_2_Cfeat_r_" + nm + ".txt")
            #dataframe_split.to_csv(save_path + "f_Concatenated_features.txt")
            if self.compute_split is True:
                #dataframe_concat.to_csv(save_path + "f_3_Concatenated_features.txt")
                for n in rng_feat:
                    feature_data = pd.DataFrame(data=split_mat[n])
                    feature_data.to_csv(save_path + "f_" + str(n+3) + "_feature_" + str(n+1) + "_r_" + nm + ".txt")
        return(dataframe_full)  

