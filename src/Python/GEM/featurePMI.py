#!/usr/bin/env python
# coding: utf-8

# In[6]:


import karateclub
from karateclub import Walklets
import networkx as nx
import random
import numpy as np
import pandas as pd
import implicit
import scipy as sc
from scipy.sparse import csc_matrix

from sklearn.preprocessing import MinMaxScaler
import math
import os
import cTimer



def get_walks(G, walk_length, dim):
    def boolean_indexing(v, walk_len, fillval=-1):
        lens = np.array([len(item) for item in v])       
        mask = lens[:,None] > np.arange(walk_len)
        out = np.full(mask.shape,fillval)
        out[mask] = np.concatenate(v)
        return out
    
    walk_n = 10
    seed=42
    model = None
    model = Walklets(dimensions = dim, seed=seed,window_size=5,epochs=10,walk_number=walk_n,walk_length=walk_length)
    model.fit(G)
    X = model.get_embedding()
    walks = None
    walks = model.walksers    
    walk_paths = boolean_indexing(walks,walk_length)
    order1 = model._select_walklets(walks,1)
    order2 = model._select_walklets(walks,2)
    
    order1 = list(filter(None, order1)) # filter out empty elements that happen due to skipping nodes in a walk for disconnected nodes
    order2 = list(filter(None, order2))
    order1 = boolean_indexing(order1,walk_length)
    order2 = boolean_indexing(order2,walk_length)
 
    walk_paths = np.concatenate((walk_paths,order1, order2), axis=0)
  
    
    return(walk_paths)


class featPMI():       
        
    def __init__(self, walks, attr_dict, fwindow_size, tlog): # a 0 to 1 scaled attrdict has to be given
        """
        Returns a structurally weighted feature_PMI array for usage in matrix factorization.
        """
        self.walks = walks        
        self.attr_dict = attr_dict
        self.fwindow_size = fwindow_size        
        self.t = tlog
        
        
    def train(self):
        self.t.start("featurePMI preprocess time: ")
        self.flatten_walks()
        self.t.stop()        
        
        self.t.start("featurePMI walk matrix time: ")
        self.estimate_walk_pmi_mat()
        self.t.stop()
        self.t.start("featurePMI feature difference matrix time: ")
        self.estimate_feature_pmi_mat()
        self.t.stop()
        self.t.start("featurePMI adding probabilities time: ")
        self.estimate_weighted_feature_pmi_mat()
        self.t.stop()
        
  
        
    def flatten_walks(self):
        # flatten the walks for faster calculations        
        paths = self.walks
        np_paths = np.array(paths)             
        short_paths = np_paths[:,list(range(1,self.fwindow_size))].astype('int')
        current_node = np_paths[:,0].astype('int') # current node is origin node.
        
        
        self.mat_size = len(set(current_node))        
        self.rows = np.repeat(current_node,self.fwindow_size-1) # The origin node of a walk         
        self.cols = short_paths.flatten() # the other nodes in the walk
        correct_indices = np.where(self.cols!=-1)[0]
        self.cols = self.cols[correct_indices]      
        self.rows = self.rows[correct_indices]       
       
    def estimate_walk_pmi_mat(self): # RETURNS transition probability matrix
        # estimate pmi matrix based on random walks (essentially equal to structure matrix, used for weighting)
        new_matrix = np.zeros([self.mat_size, self.mat_size])
        np.add.at(new_matrix, (self.rows, self.cols), 1)
        new_matrix = new_matrix.astype('float64')
        # approximate PMI matrix
        max_count = np.max(new_matrix,axis=1)+1        
        # introduce probabilities
        self.pmi_matrix = new_matrix/max_count        
        
        
    def estimate_feature_pmi_mat(self):
        # estimate the incomplete feature pmi matrix by random walks.
        # these random walks determine which pairwise feature differences are sampled
        n_features = len(self.attr_dict[0])         
        self.feature_PMI = np.zeros([n_features,self.mat_size,self.mat_size])
        self.cFeature_PMI = np.zeros([self.mat_size,self.mat_size])
        self.feature_PMI = self.feature_PMI.astype('float64')
        self.cFeature_PMI = self.cFeature_PMI.astype('float64')
        for i in range(len(self.rows)): # for all nodes
                 
        
            row = self.rows[i]
            row_attr = self.attr_dict[row]
            col = self.cols[i]
            col_attr = self.attr_dict[col]            
            
            #self.cFeature_PMI[row,col] = distance.euclidean(row_attr, col_attr)
                
            for it in range(n_features): # for all features     CHECK          
                value = (row_attr[it]-col_attr[it])**2        ##or sq      
                self.feature_PMI[it,row,col] = value
            
            
            #Add differences and sum, larger distrances penalized
            ## quicker with np.multiply element wise probs
       
        self.cFeature_PMI = sum([x**2 for x in self.feature_PMI]) # # combined features PMI matrix -> distance
        
                
    def estimate_weighted_feature_pmi_mat(self):
        # normalize and weight the feature PMI matrix
       
        
        fPMI_mat = self.feature_PMI/(self.feature_PMI.sum(axis=1,keepdims=True)+1e-6) # small epsilon for division by zero 
        
        cfPMI_mat = self.cFeature_PMI/(self.cFeature_PMI.sum(axis=1,keepdims=True)+1e-6)        
      
        # weight the pmi feature matrix by the structure matrix
        self.weight_feature_PMI = fPMI_mat*self.pmi_matrix
        self.weight_cFeature_PMI = cfPMI_mat*self.pmi_matrix
        
        
       
        
    def estimate_embeddings(self, emb_dim = 2):        
        self.t.start("featurePMI generate embedding time: ")        
        self.emb_dim = emb_dim
        n_features = len(self.attr_dict[0])
        embeddings = np.zeros([n_features,self.mat_size,emb_dim])
        cEmbeddings = np.zeros([self.mat_size,emb_dim])
        alpha = 15
        
        mat = sc.sparse.csc_matrix(self.weight_cFeature_PMI) 
        user_vecs, item_vecs = implicit.alternating_least_squares((mat*alpha).astype('double'), factors=emb_dim, 
                                                          regularization = 0.1, iterations = 200)
        self.cEmbeddings = user_vecs
       
        
        for it in range(n_features):
            mat = sc.sparse.csc_matrix(self.weight_feature_PMI[it])  
            user_vecs, item_vecs = implicit.alternating_least_squares((mat*alpha).astype('double'), factors=emb_dim, 
                                                          regularization = 0.1, iterations = 200)
            
            embeddings[it] = user_vecs        
       

        self.embeddings = embeddings
        self.t.stop()
        self.t.add_total()
        print("Completion time: " + str(self.t.get_log()[-1][1]))
  
        
    def get_probs(self):
        return(self.pmi_matrix)
    
    def get_fpmi_mat(self):
        return(self.feature_PMI)
    
    def get_cfpmi_mat(self):
        return(self.cFeature_PMI)
    
    def get_weighted_pmi_mat(self):
        return(self.weight_feature_PMI)
    
    def get_embeddings(self):
        return(self.embeddings)
    
    def get_pandas(self, save = False, save_path = "", nm = ""): 
        size = self.emb_dim*len(self.attr_dict[0])
        n_nodes = np.shape(self.embeddings)[1]
        dim = range(0,self.emb_dim)
        n_feat = range(0,len(self.attr_dict[0]))
        
        
        new_cols = ['feat_' + str(n+1) + '_dim_' + str(d+1) for n in n_feat for d in dim]
        full_embedding = np.swapaxes(self.embeddings,0,1) # swap and flatten 1 axis to join all embeddings into 1 embedding
        full_embedding = np.reshape(self.embeddings,(n_nodes,size))
        dataframe = pd.DataFrame(data=full_embedding, columns=new_cols)
        
        dataframe_combined = pd.DataFrame(data=self.cEmbeddings)
    
        
        
        if save: # For saving: combined embedding, structure embedding, feature embeddings            
            print("saving embedding to: ", save_path)
            dataframe_combined.to_csv(save_path+"f_2_Cfeat_r_" + nm + ".txt")
            #dataframe.to_csv(save_path + "f_3_Concatenated_features_" + nm + ".txt" )
            for n in n_feat:
                feature_data = pd.DataFrame(data=self.embeddings[n])
                feature_data.to_csv(save_path + "f_" + str(n+3) + "_feature_" + str(n+1) + "_r_" + nm + ".txt")
        return(dataframe)
        

