#!/usr/bin/env python
# coding: utf-8

# In[143]:


import time
import os
import pandas as pd

class TimerError(Exception):

    """A custom exception used to report errors in use of Timer class"""


class Timer:

    def __init__(self, log_t = False):
        self._start_time = None
        self.log = []
        self.log_t = log_t

    def start(self, custom_text = "Time in s: "):
        self.custom_text = custom_text # add custom identifiers for the final log

        """Start a new timer"""
        if self._start_time is not None:
            raise TimerError(f"Timer is running. Use .stop() to stop it")
        self._start_time = time.perf_counter()


    def stop(self):
        """Stop the timer, and report the elapsed time"""
        if self._start_time is None:
            raise TimerError(f"Timer is not running. Use .start() to start it")
        elapsed_time = time.perf_counter() - self._start_time
        self._start_time = None
        if self.log_t is True:
            self.log.append([self.custom_text, elapsed_time])
        print(self.custom_text + f"{elapsed_time:0.4f} seconds")
        
    def add_total(self):
        if self.log_t is True:
            """Add final entry with total time"""
            string = str.split(self.custom_text)[0] + " total time: "
            names,values = zip(*self.log) 
            self.log.append([string,sum(values)])
        else:
            raise TimerError("Logging is not enabled, enable logging with log_t = True on object creation")
        
    def get_log(self):        
        return(self.log)
    
    def log_save(self, logpath, technique): # saves a log of the running time
        if not os.path.exists(logpath):
            os.makedirs(logpath)
            print("logdir did not exist, creating..")

        log = pd.DataFrame(self.log, columns=["phases","time in s"])
        log.to_csv(logpath + technique + ".txt")
        print("Saving log to: " + logpath + technique + ".txt")    

