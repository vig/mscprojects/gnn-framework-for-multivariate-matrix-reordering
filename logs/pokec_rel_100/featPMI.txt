,phases,time in s
0,featurePMI walk_paths/structure time: ,3.178359300000011
1,featurePMI preprocess time: ,0.0013278000000127577
2,featurePMI walk matrix time: ,0.0019295999999826563
3,featurePMI feature difference matrix time: ,0.11493090000001871
4,featurePMI adding probabilities time: ,0.00034679999998843414
5,featurePMI generate embedding time: ,0.8117158999999958
6,featurePMI total time: ,4.108610300000009
