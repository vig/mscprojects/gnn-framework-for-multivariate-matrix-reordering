# Graph2DMatrix

Creating 2D matrix visualizations of simple and complex graph structures. With this framework it is possible to create 2 dimensional high level overview images of complex graph structures. It is possible to embedd basic, weighted, directed and multivariate graphs (only binary/numerical attributes) and visualize these graphs in a 2D matrix. In [powerpoint](./src/presentations/Colloquium Presentation.pdf) a quick overview is given, while in [Master thesis](./thesis/MbiThesis_Job_Heuvelmans_Final.pdf) a more elaborate overview of this work is given.

## Visuals

### Barbell graph with networkx
<img src="/blob/barbell.png"  width="50%" height="50%">

### Embedding visualized
<p float="left">
    <img src= "./blob/All experiments (Unstructured)/Embedding vis/PCA_attentionWalk_3.png" width="30%" height="30%">
    <img src= "./blob/All experiments (Unstructured)/Embedding vis/PCA_struc2vec_2.png"  width="30%" height="30%">
    <img src= "./blob/All experiments (Unstructured)/Embedding vis/PCA_Walklets_1.png"  width="30%" height="30%">
</p>

### Unordered matrix vs ordered matrix
<p float="left">
    <img src="/blob/All experiments (Unstructured)/example_table/Matrix.png"  width="45%" height="45%">
    <img src="/blob/All experiments (Unstructured)/example_table/Matrix2.png"  width="45%" height="45%">
</p>

### After seriation: Resulting barbell graph embeddings
<img src="/blob/barbell_all.png"  width="80%" height="80%">

## Installation

#### Step 1: Setting up virtual environments
This framework uses multiple (algorithmic) packages with different dependencies. Therefore, to run anything beyond the bare minimal version of this framework, we recommend to set up multiple virtual environments. The framework utilized [Anaconda](https://www.anaconda.com/) for management of these environments and [Jupyter Notebook](https://jupyter.org/) for development of Python scripts. See documentation on Anaconda to create the virtual environments, activate the environments and correct installment of packages in the environments.

#### Step 2: Installing required packages
The bare minimal version of this framework can be run with just the requirements of the GEM package for Python and the R requirements for R. 

This environment is created with the [GEM package by Palesh](https://github.com/palash1992/GEM) and includes the following requirements: [requirements GEM](./src/Python/GEM/GEM_requirements.txt). Once the environment is activated in Anaconda and the right installation folder is selected (in Anaconda) we can install the requirements by the following command: 
<br/>
``conda install --file YOURFOLDER/src/Python/GEM/GEM_requirements.txt``

The R environment has the following dependencies: [R](./src/R/Requirements_R.txt). These can be installed by running or copying the contents of the prespecified textfile.

To include the algorithm attentionWalk a Python environment for [attentionwalk package by Benedekrozemberczki](https://github.com/benedekrozemberczki/AttentionWalk) with requirments: [attentionWalk](./src/Python/attentionWalk/attentionWalk_requirements.txt) should be made. To install, run the following command in a different environment:
<br/>
``conda install --file YOURFOLDER/src/Python/attentionWalk/attentionWalk_requirements.txt``

To include the algorithm struc2vec a Python environment for [struc2vec package by Leoribeiro](https://github.com/leoribeiro/struc2vec) with requirements: [struc2vec](./src/Python/struc2vec/struc2vec_requirements.txt) should be made. To install, run the following command in a different environment:
<br/>
``conda install --file YOURFOLDER/src/Python/struc2vec/struc2vec_requirements.txt``

#### Step 3: Replace files
Unfortunately, some packages required a small adjustment in source code (e.g. allow for weighted graph embedding), or needed to be changed for any other reason. Therefore, after installing the required packages we need to place the replacement files in [replacement_files-GEM](./src/Python/GEM/replacement_files) into their respective folder. The same holds for [replacement_files-attentionWalk](./src/Python/attentionWalk/replacement_files) and [replacement_files-struc2vec](./src/Python/struc2vec/replacement_files).  These folders include a README.txt for more information of where to place these files.

#### Step 4 (Optional): Convert existing graphs
##### Embedding phase
This framework uses the edge list for representing graphs. Each line in this edge list is a node to node edge separated by a comma or space and optionally includes the weight of this edge in the third column. For multivariate graph embedding, an extra attribute file has to be generated which shows a node and their attributes on a single comma separated line. In the data repository there is an example of this representation

##### Visualization phase
After embedding, the embeddings are saved to an n-dimensional comma separated txt file and, afterwards, this file is loaded into the R script.



## Usage Explanation
#### Dataset selection, data loading and storing
Due to the inherent complexity of the analysis, the current version only includes a jupyter notebook to run the code. For all notebooks: change the path variable(s) before running the code. Each of the scripts have several variables to be specified in the beginning. In the Python scripts dataset names are loaded from a single txt file in: 

> YOURPATH/input/names_datasets.txt

the edge paths (or graph edges) should be stored in: 

> YOURPATH/input/dataset_graph.txt. 
>
> The word 'graph' can be replaced by any other dataset name.

And finally, outputs are stored in:

> YOURPATH/output/embedding_algorithm.txt. 
>
> The word 'embedding_algorithm' is replaced by any of the embedding algorithms.

#### Embedding with GEM, attentionWalk and struc2vec
To embedd on the GEM-package, open [GEM.ipynb](./src/Python/GEM/GEM.ipynb), set the starting variables including the names of the models to run. Run the decorator_fun function with prespecified number_of_runs, dataset(s) and models.
To embedd on the attentionWalk package, open [attentionWalk.ipynb](./src/Python/GEM/attentionWalk.ipynb) and edit the path and datasets to run on. Run the decorator_fun function with variable epochs, dimensions and iterations.
Struc2vec can be run by opening the [struc2vec.ipynb](./src/Python/GEM/struc2vec.ipynb) and editing the path and datasets. Run the decorator_fun function with variable dimensions and iterations.

#### Embedding Weighted
Similar as embedding a basic graph. The only change is that the input changes from an edgelist `node node` to a weighted edgelist `node node weight`. Algorithms in the embedding phase use the weights automatically when these are supplied to them.

#### Embedding  Multivariate
To embedd on the multivariate graph embedding algorithms featWalk and featPMI run the following nodebook: [Multivariate embeddings.ipynb](./src/Python/GEM/Multivariate embeddings.ipynb) Place the attributes in [attribute folder](./data/input/attributes) and name the attribute file as follows: datasetname_attr, where 'datasetname' is replaced by your dataset name. The structure of the attributes is similar to the edgelist: `attribute_1, attribute_2, attribute_x`. It is possible to supply both numerical and binary values. The output of the algorithms is structured as follows: structure embedding (Struct), combined features (Cfeat) embedding and individual feature embeddings (feature). See [Master thesis](./thesis/MbiThesis_Job_Heuvelmans_Final.pdf) for more details.

#### Running R
Run [preprocess.R](./src/R/preprocess.R) to reduce the high dimensions of the embeddings to two dimensions. This results in new two dimensional embeddings in new documents. These documents are loaded into the [Seriation.R](./src/R/Seriation.R) script, transformed to a distance matrix and afterwards a seriation algorithm is applied to retrieve a good ordering. It is possible to view the images by plotting the images in the plot_list.

Finally, there is the [master_multi.R](./src/R/master_multi.R) to run the script multiple times, and there is the [shinyApp.R](./src/R/shinyapp.R), a rough visualization tool for the reorderings.

## Basic Usage example
We selected the basic barbell graph and embedded this graph with all embedding algorithms implemented in this framework.
#### Barbell graph to be embedded
<img src="/blob/barbell.png"  width="50%" height="50%">

The following settings have to be adjusted to embedd the barbell graph:

```python
# Variables to change GEM
datasets = ["barbell"] # dataset name in YOURPATH/data/input/dataset_barbell.txt
source_path = "YOURPATH" # Change to yourpath

dim_RW = 12 # emb_dim Random Walk
dim_MF = 4 # emb_dim Matrix factorization
dim_DL = 2 # emb_dim Deep learning
walk_n = 10 # walk_number
walk_l = 80 # walk_length

decorator_fun(run_on_models,5,datasets,models)()
```
```python
# Variables to change attentionWalk
source_path = "YOURPATH" # input_path
dataset = ["barbell"]
decorator_fun(attention_Walk, source_path, dataset, 200, 12,5)() # Run multiple times

```
```python
# Variables to change struc2vec
source_path = "YOURPATH"
dataset = ['barbell']
decorator_fun(struc_2vec,source_path,dataset,12,5)() # run multiple times

```
All output is saved under YOURPATH/data/output/barbell/embedding_x.txt. For now it is necessary to create this folder yourself. The n-dimensional embeddings are loaded into [preprocess.R](./src/R/preprocess.R). 

```R
# To select the sourcepath, datasets to preprocess and the three preprocessing techniques.
path <- "YOURPATH/data/output"
datasets <- c("barbell")
techniques <- c("PCA","UMAP","TSNE")
preprocess(path,datasets,techniques) # preprocess data
```
The two-dimensional output is saved under YOURPATH/data/output/barbell/preprocessed/dim_red_technique/embedding_x.txt with 'dim_red_technique' being PCA, TSNE or UMAP. To seriate the results open [Seriation.R](./src/R/Seriation.R) and adjust the following variables.

```R
# To run, change the following variables to your liking. Output is the seriated plots.
dataset = "barbell"
seriation_method <- "linkern"
dim_red = "PCA" # PCA, TSNE or UMAP
path = "YOURPATH/data/output"
distance_metric <- "euclidean" # Euclidean or Manhattan
doScale <- TRUE # Scale between 0 and 1
removeOutliers <- TRUE # remove 10% outliers
methods <- c("attentionWalk","deepwalk","HOPE","Laplacian_Eigenmaps","Locally_Linear_embedding","node2vec","SDNE","struc2vec","Walklets")
# Concorde (if used) has to be installed prior from (https://www.math.uwaterloo.ca/tsp/concorde/downloads/downloads.htm)
concorde_path("YOURPATH")
plotMethod <- "univariateSeriation" # Switch between different selection of best plot criteria

## Plot with plot_list[[x]] where x is the number you want to plot, or in a grid (does not work for multivariate cases) as below
post_grobs <- plot_best(topIndices, plot_list,nCol=3)
plot(post_grobs)
```
The output is given back in a plot_list and the orders are also saved in an order_list. This example resulted in the following plot:

#### Resulting barbell graph embeddings
<img src="/blob/barbell_all.png"  width="80%" height="80%">

## Troubleshooting
`Error preprocessing:` SDNE overfitted with only zeroes in the embedding
